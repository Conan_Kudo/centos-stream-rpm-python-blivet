#!/bin/bash

# Bail out if anything goes wrong
set -e

# Get path to blivet
blivet_path=$(find /usr/ -path */site-packages/)

# Run upstream unit tests
cd ./source/
PYTHONPATH=$PYTHONPATH:$blivet_path python3 -m unittest discover -v -s ./tests/ -p '*_test.py'

